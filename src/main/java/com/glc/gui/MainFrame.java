package com.glc.gui;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;

public class MainFrame extends JFrame {
    public MainFrame() throws ClassNotFoundException, SQLException {
        //Container c=getContentPane();
        setVisible(true);
        setTitle("仓库管理系统");
        setSize(1000, 700);
        //创建系统该默认组件工具包
        Toolkit tool = Toolkit.getDefaultToolkit();
        //获取屏幕尺寸，赋给一个二维坐标对象
        Dimension d = tool.getScreenSize();
        //让主窗体在屏幕中间显示
        setLocation((d.width - getWidth()) / 2, (d.height - getHeight()) / 2);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        setLayout(new GridLayout(2, 1));


        JMenuBar mb = new JMenuBar();


        Table table = new Table();
        JMenu jmenut = table.gettable();
        mb.add(jmenut);
        Help help = new Help();
        JMenu jmenu = help.getjmenu();
        mb.add(jmenu);
        this.setJMenuBar(mb);
        Directory_Tree directory_Tree = new Directory_Tree();
        Stock_c stock_c = new Stock_c();
        JScrollPane jtable = new JScrollPane(stock_c.stock_setdata());
        JTabbedPane p = new JTabbedPane();
        p.add("清单列表", jtable);
        p.add("目录树", new JScrollPane(directory_Tree));

        add(p);

        validate();
        // pack();
        JTabbedPane p2 = new JTabbedPane();

        p2.add("进货", new Deal(1));
        p2.add("退货", new Deal(-1));
        p2.add("退料", new Deal(1));
        p2.add("领用", new Deal(-1));

        add(p2);


    }
	/*public static void main(String[] args) throws ClassNotFoundException, SQLException {
		new MainFrame();
	}
	*/


}
