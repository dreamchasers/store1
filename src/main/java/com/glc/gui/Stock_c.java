package com.glc.gui;


import com.glc.utils.JDBCUtil;

import javax.swing.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Stock_c extends JPanel{
    Statement stmt=null;
    ResultSet res=null;
    Connection c2=null;

    public void Stock_c() throws ClassNotFoundException,SQLException
    {

        add(new JScrollPane(stock_setdata()));
        //setSize(400,400);


    }
    public 	 JTable stock_setdata() throws ClassNotFoundException, SQLException
    {		c2= JDBCUtil.getConnection();
        Statement stmt=c2.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_UPDATABLE);
        int j=-1;


        String[]  name= {"库存ID","仓库id","材料名称","数量","单价","总金额"};
        String sql="select * from stock ";
        res=stmt.executeQuery(sql);
        res.last();
        String[][] d=new String[res.getRow()][6];
        res.first();

        do
        {

            ++j;
            for(int i=1;i<=6;i++)
            {
                d[j][i-1]=res.getString(i);
            }


        }while(res.next());
        JTable jTable=new JTable(d,name);

        return jTable;


    }
}