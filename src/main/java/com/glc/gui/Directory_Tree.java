package com.glc.gui;

import com.glc.utils.JDBCUtil;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Directory_Tree extends JPanel
{
    public Directory_Tree() throws ClassNotFoundException, SQLException
    {



        DefaultMutableTreeNode zero=new DefaultMutableTreeNode(null);
        DefaultMutableTreeNode one=new DefaultMutableTreeNode(null);
        DefaultMutableTreeNode two=new DefaultMutableTreeNode(null);
        DefaultMutableTreeNode three=new DefaultMutableTreeNode(null);
        DefaultMutableTreeNode four=new DefaultMutableTreeNode(null);
        DefaultMutableTreeNode all=new DefaultMutableTreeNode("目录树");
        JTree top=new JTree(all);
        // JScrollPane jscp=new JScrollPane(top);

        setSize(5000,4000);

        add(top);
        Statement stmt=null;
        ResultSet res=null;
        Connection c2=null;
        c2= JDBCUtil.getConnection();
        stmt=c2.createStatement();

        res=stmt.executeQuery(" select mc,ggxh,hh from clggb");
        while(res.next())
        {
            String  mc=res.getString(1);
            String  ggxh=res.getString(2);
            String  hh=res.getString(3);

            int len=hh.length()/2;
            switch(len)

            {
                case 0:{ zero=new DefaultMutableTreeNode(mc);all.add(zero);};break;
                case 1:{ one=new DefaultMutableTreeNode(mc);zero.add(one);};break;
                case 2:{ two=new DefaultMutableTreeNode(mc);one.add(two);};break;
                case 3:{ three=new DefaultMutableTreeNode(mc);two.add(three);};break;
                case 4:{ four=new DefaultMutableTreeNode(mc+","+ggxh);three.add(four);};break;
            }
        }

        if (c2!=null)

        {
            c2.close();
        }
        if(stmt!=null)
        {
            stmt.close();
        }
        if(res!=null)
        {
            res.close();
        }



    }
}







