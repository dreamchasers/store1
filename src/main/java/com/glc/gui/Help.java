package com.glc.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Help
{

    public JMenu getjmenu()
    {
        JMenu jmenu=new JMenu("帮助");

        JMenuItem j1=new JMenuItem("关于");

        jmenu.add(j1);
        j1.addActionListener(new ACT());


        return jmenu;

    }
    class ACT extends JFrame implements ActionListener
    {


        @Override
        public void actionPerformed(ActionEvent arg0) {
            // TODO Auto-generated method stub
            new About();

        }

    }
    class About extends JDialog
    {
        public About()
        {
            setVisible(true);
            setTitle("关于");
            setSize(200,100);
            Toolkit tool=Toolkit.getDefaultToolkit();
            Dimension d=tool.getScreenSize();
            setLocation((d.width-getWidth())/2,(d.height-getHeight())/2);
            setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            JLabel jlabel=new JLabel("出品人：甘卢宸，有问题请联系");
            add(jlabel);
            validate();

        }
    }
}
