package com.glc.gui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;

import com.glc.utils.JDBCUtil;


public class Table
{
    public JMenu gettable()
    {
        JMenu jmenu=new JMenu("表格");

        JMenuItem j1=new JMenuItem("业务表");
        JMenuItem j2=new JMenuItem("库存表");
        JMenuItem j3=new JMenuItem("仓库信息表");
        JMenuItem j4=new JMenuItem("用户表");


        jmenu.add(j1);
        jmenu.add(j2);
        jmenu.add(j3);
        jmenu.add(j4);

        String[] s1= {"仓库id","材料名称","数量","单价","总金额","业务类型","时间"};
        String[] s2= {"库存ID","仓库id","材料名称","数量","单价","总金额"};
        String[] s3= {"仓库id","仓库名","仓库地址"};
        String[] s4= {"用户id","用户名称","密码"};

        j1.addActionListener(new ACT3("standing_book",s1,7));
        j2.addActionListener(new ACT3("stock",s2,6));
        j3.addActionListener(new ACT3("store_info",s3,3));
        j4.addActionListener(new ACT3("user",s4,3));

        return jmenu;

    }
    class ACT3 implements ActionListener
    {
        int column_num=0;
        String sql;
        String[] column;
        Connection c2;
        Statement stmt;
        public ACT3(String s,String[] tem,int num)
        {
            String name;
            column_num=num;
            column=tem;
            name=s;
            sql="select * from "+s;
            c2= JDBCUtil.getConnection();
            try
            {
                stmt=c2.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_UPDATABLE);
            } catch (SQLException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        @Override
        public void actionPerformed(ActionEvent e) {
            // TODO Auto-generated method stub
            try
            {
                new Table_stock(sql,column,column_num);
            } catch (SQLException e1)
            {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }


        }
        class Table_stock extends JFrame
        {
            ResultSet res=null;
            public Table_stock(String sqls,String[] column,int number) throws SQLException
            {
                setVisible(true);
                setSize(500,300);

                Toolkit tool=Toolkit.getDefaultToolkit();//创建系统该默认组件工具包
                Dimension d=tool.getScreenSize();//获取屏幕尺寸，赋给一个二维坐标对象
                //让主窗体在屏幕中间显示
                setLocation((d.width-getWidth())/2,(d.height-getHeight())/2);
                setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                try
                {
                    res=stmt.executeQuery(sqls);
                } catch (SQLException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                res.last();
                String[][] data=new String[res.getRow()][number];
                res.first();
                int jj=-1;
                do
                {
                    ++jj;
                    for(int i=1;i<=number;i++)
                    {
                        data[jj][i-1]=res.getString(i);
                    }
                }while(res.next());

                JTable j=new JTable(data,column);
                add(new JScrollPane(j));
            }

        }

    }
}
