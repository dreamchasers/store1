package com.glc.gui;


import com.glc.entity.StandingBook;
import com.glc.service.OutStoreService;
import com.glc.utils.JDBCUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

public class Deal extends JPanel {

    JTextField mname;
    JTextField storeId;
    JTextField mcount;
    JTextField mprice;
    JTextField msum;

    /**
     * @param t 1表示入库，-1表示出库
     */
    public Deal(int t) {

        GridLayout grid = new GridLayout(4, 5, 0, 50); // 网格布局
        setLayout(grid);

        JLabel jlabel1 = new JLabel("请输入材料名称: ");
        mname = new JTextField();
        JLabel jlabel01 = new JLabel();
        mname.setColumns(40);
        JLabel jlabel02 = new JLabel();
        JLabel jlabel2 = new JLabel("请输入仓库编号：");
        storeId = new JTextField();
        storeId.setColumns(40);
        JLabel jlabel03 = new JLabel();
        JLabel jlabel3 = new JLabel("请输入数量：");
        mcount = new JTextField();
        mcount.setColumns(40);
        JLabel jlabel04 = new JLabel();
        JLabel jlabel4 = new JLabel("请输入单价:");
        mprice = new JTextField();
        mprice.setColumns(40);
        JLabel jlabel05 = new JLabel();
        JLabel jlabel5 = new JLabel("请输入总金额:");
        msum = new JTextField();
        msum.setColumns(40);
        JLabel jlabel06 = new JLabel();
        JButton jbutton = new JButton("提交");
        JLabel jlabel07 = new JLabel();
        JLabel jlabel08 = new JLabel();
        jbutton.setSize(200, 100);
        jbutton.addActionListener(new ACT1(t));


        add(jlabel1);
        add(mname);
        add(jlabel02);
        add(jlabel2);
        add(storeId);

        add(jlabel3);
        add(mcount);
        add(jlabel01);
        add(jlabel4);
        add(mprice);


        add(jlabel5);
        add(msum);
        add(jlabel03);
        add(jbutton);
        add(jlabel05);

        add(jlabel04);
        add(jlabel06);
        add(jlabel07);
        add(jlabel08);
        validate();

    }

    class ACT1 implements ActionListener {
        Statement state = null;
        ResultSet res = null;
        Connection con = null;
        int flag = 1;

        public ACT1(int tnum) {
            flag = (tnum == 1 ? tnum : -1);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            StandingBook standingBook = new StandingBook();
            standingBook.setMname(mname.getText());
            standingBook.setTime(new Date());
            if (storeId.getText() != "") {
                standingBook.setStoreId(Integer.parseInt(storeId.getText().trim()));
            }
            if (mcount.getText() != "") {
                standingBook.setMcount(Double.parseDouble(mcount.getText().trim()));
            }

            if (mprice.getText() != "") {
                standingBook.setMprice(Double.parseDouble(mprice.getText().trim()));
            }
            if (msum.getText() != "") {
                standingBook.setMsum(Double.parseDouble(msum.getText().trim()));
            }
            OutStoreService storeService = new OutStoreService();
            if (flag == -1) {
                standingBook.setType("出库");
                try {
                    storeService.out(standingBook);
                    new Commit(1);
                    //捕获从服务层抛出的异常，如果捕获到说明此时执行了回滚操作
                } catch (Exception exception) {
                    new Commit(2);
                    exception.printStackTrace();
                }
            } else {
                standingBook.setType("入库");
                try {
                    storeService.in(standingBook);
                    new Commit(1);
                } catch (Exception exception) {
                    new Commit(2);
                    exception.printStackTrace();
                }
            }

        }

        class Commit extends JFrame {
            public Commit(int t) {
                String s = "unknown";
                setVisible(true);
                setTitle("事务是否已提交");
                setSize(300, 200);
                Toolkit tool = Toolkit.getDefaultToolkit();
                Dimension d = tool.getScreenSize();
                setLocation((d.width - getWidth()) / 2, (d.height - getHeight()) / 2);
                setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                if (t == 1) {
                    s = "业务成功已提交！";
                }
                if (t == 2) {
                    s = "提交过程中出现错误，事务已回滚！";
                }
                JLabel jlabel = new JLabel(s);
                add(jlabel);
                validate();
                pack();

            }
        }


    }
}
