package com.glc.Dao;

import com.glc.entity.Stock;
import com.glc.utils.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class StockDao {
    static final StockDao STOCK_DAO=new StockDao();

    public static StockDao getStockDao() {
        return STOCK_DAO;
    }
    private StockDao(){

    }

    public static void main(String[] args) {
//        StockDao stockDao=getStockDao();
//        Stock stock=new Stock(3,1,"kid",5,1D,1.0);
        //System.out.println(stockDao.addStock(stock));
        //System.out.println(stockDao.deleteStock(1));
        //System.out.println(stockDao.updateStock(stock));

    }

    public boolean addStock(Connection conn,Stock stock){
        Connection connection= JDBCUtil.getConnection();
        PreparedStatement ps=null;
        String sql="insert into stock values (null ,"+stock.getStoreId()
                + ",'"+stock.getSname()+"',"+stock.getCount()+","+stock.getPrice()+","
                +stock.getSum()+")";
        try {
            ps=connection.prepareStatement(sql);
            return ps.executeUpdate()==1;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            try {
                ps.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return false;
    }
    public boolean deleteStock(Connection conn,Integer id){
        Connection connection= JDBCUtil.getConnection();
        PreparedStatement ps=null;
        String sql="delete from stock where id=?";
        try {
            ps= connection.prepareStatement(sql);
            ps.setInt(1,id);
            return ps.executeUpdate()==1;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            try {
                ps.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return false;
    }
    public boolean updateStock(Connection conn,Stock stock){
        PreparedStatement ps=null;
        StringBuilder sb=new StringBuilder("update stock set");
        if (stock.getStoreId()!=null){
            sb.append(" store_id="+stock.getStoreId()+",");
        }
        if(stock.getSname()!=null){
            sb.append("sname='"+stock.getSname()+"',");
        }
        if(stock.getCount()!=null){
            sb.append("count="+stock.getCount()+',');
        }
        if(stock.getPrice()!=null){
            sb.append("price="+stock.getPrice()+',');
        }
        if(stock.getSum()!=null){
            sb.append("sum="+stock.getSum()+',');
        }
        sb.deleteCharAt(sb.length()-1);
        sb.append(" where id="+stock.getId());
        try {
            ps= conn.prepareStatement(sb.toString());
            return ps.executeUpdate()==1;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            try {
                ps.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return false;
    }
    public Stock queryStockById(Connection coon,Integer id){
        Connection connection= JDBCUtil.getConnection();
        PreparedStatement ps=null;
        String sql="select * from stock where id="+id;
        try {
            ps= connection.prepareStatement(sql);
            ResultSet rs=ps.executeQuery();
            rs.next();
            return new Stock(rs.getInt("id"),rs.getInt("store_id"),rs.getString("sname"),
                    rs.getDouble("count"),rs.getDouble("price"),rs.getDouble("sum"));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            try {
                ps.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return null;
    };
    public List<Stock> queryStockByCondition(Connection conn,Stock stock){
        PreparedStatement ps=null;
        StringBuilder sb=new StringBuilder("select * from stock where 1=1"+stock.queryString());
        List<Stock> result=new LinkedList<>();
        try {
            ps= conn.prepareStatement(sb.toString());
            ResultSet rs=ps.executeQuery();
            while (rs.next()){
                result.add(new Stock(rs.getInt("id"),rs.getInt("store_id"),rs.getString("sname"),
                        rs.getDouble("count"),rs.getDouble("price"),rs.getDouble("sum")));
            }
            return result;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            try {
                ps.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return null;
    };
}
