package com.glc.service;

import com.glc.Dao.StandingBookDao;
import com.glc.Dao.StockDao;
import com.glc.entity.StandingBook;
import com.glc.entity.Stock;
import com.glc.utils.JDBCUtil;

import java.sql.Connection;
import java.util.List;

public class OutStoreService {
    static StandingBookDao standingBookDao=StandingBookDao.getStandingBook();
    static StockDao stockDao=StockDao.getStockDao();
    /**
     * 入库服务
     * @param standingBook 记录信息
     * @return 返回是否插入成功
     */
    public boolean in(StandingBook standingBook) throws Exception {
        Connection conn= JDBCUtil.getConnection();
        try {
            conn.setAutoCommit(false);
            standingBookDao.addOneRecord(conn,standingBook);
            Stock temp=new Stock();
            temp.setSname(standingBook.getMname());
            temp.setStoreId(standingBook.getStoreId());
            List<Stock> stocks=stockDao.queryStockByCondition(conn,temp);
            Stock stock=stocks.get(0);
            stock.setCount(stock.getCount()+standingBook.getMcount());
            stock.setSum(stock.getSum()+standingBook.getMsum());
            stock.setPrice(1.0*stock.getSum()/stock.getCount());
            stockDao.updateStock(conn,stock);
            //提交事务
            conn.commit();
            return true;

        } catch (Exception e) {
            //捕获异常后打印异常信息
            e.printStackTrace();
            try {
                //出错回滚
                conn.rollback();
            } catch (Exception throwables) {
                throwables.printStackTrace();
            }
            //为了防止耦合，执行回滚操作后继续向上抛异常
            throw e;
        }finally {
            JDBCUtil.release(conn);
        }
    }
    /**
     * 出库服务
     * @param standingBook 记录信息
     * @return 返回是否插入成功
     */
    public  boolean out(StandingBook standingBook) throws Exception {
        Connection conn= JDBCUtil.getConnection();
        try {
            //开启事务（关闭自动提交）
            conn.setAutoCommit(false);
            Stock temp=new Stock();
            temp.setSname(standingBook.getMname());
            temp.setStoreId(standingBook.getStoreId());
            List<Stock> stocks=stockDao.queryStockByCondition(conn,temp);
            Stock stock=stocks.get(0);
            if (stock.getCount()>standingBook.getMcount()){
                stock.setSum(stock.getSum()-standingBook.getMsum());
                stock.setCount(stock.getCount()-standingBook.getMcount());
                stock.setPrice(1.0*stock.getSum()/stock.getCount());
                stockDao.updateStock(conn,stock);
                standingBookDao.addOneRecord(conn,standingBook);
            }else {
                throw new Exception("库存不足");
            }
            //提交事务
            conn.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                //出错回滚
                conn.rollback();
            } catch (Exception throwables) {
                throwables.printStackTrace();
            }
            //为了防止耦合，执行回滚操作后继续向上抛异常
            throw e;
        }finally {
            JDBCUtil.release(conn);
        }
    }

}
