package com.glc.entity;

import com.glc.utils.DateUtil;

import java.util.Date;

/**
 * standing_book
 * 
 * @author bianj
 * @version 1.0.0 2021-05-15
 */
public class StandingBook implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 4830687650657196L;

    /** 仓库记录ID */
    private Integer id;

    /** 货物名称 */
    private String mname;

    /** 仓库ID */
    private Integer storeId;

    /** 货物数量 */
    private Double mcount;

    /** 货物单价 */
    private Double mprice;

    /** 货物总额 */
    private Double msum;

    /** 业务类型 */
    private String type;

    /** 时间 */
    private Date time;

    public StandingBook() {
    }

    public StandingBook(Integer id, String mname, Integer storeId, Double mcount, Double mprice, Double msum, String type, Date time) {
        this.id = id;
        this.mname = mname;
        this.storeId = storeId;
        this.mcount = mcount;
        this.mprice = mprice;
        this.msum = msum;
        this.type = type;
        this.time = time;
    }

    public String queryString() {
        StringBuilder sb=new StringBuilder();
        if (id!=null){
            sb.append(" and id="+id);
        }
        if (mname!=null){
            sb.append(" and mname='"+mname+"'");
        }
        if (storeId!=null){
            sb.append(" and store_id="+storeId);
        }
        if (mcount!=null){
            sb.append(" and mcount='"+mcount+"'");
        }
        if (mprice!=null){
            sb.append(" and mprice="+mprice);
        }
        if (msum!=null){
            sb.append(" and msum="+msum);
        }
        if (type!=null){
            sb.append(" and mname="+type);
        }
        if (time!=null){
            sb.append(" and time='"+DateUtil.toTimeString(time)+"'");
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        return "StandingBook{" +
                "id=" + id +
                ", mname='" + mname + '\'' +
                ", store_id=" + storeId +
                ", mcount=" + mcount +
                ", mprice=" + mprice +
                ", msum=" + msum +
                ", type=" + type +
                ", time=" + time +
                '}';
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public Double getMcount() {
        return mcount;
    }

    public void setMcount(Double mcount) {
        this.mcount = mcount;
    }

    public Double getMprice() {
        return mprice;
    }

    public void setMprice(Double mprice) {
        this.mprice = mprice;
    }

    public Double getMsum() {
        return msum;
    }

    public void setMsum(Double msum) {
        this.msum = msum;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}