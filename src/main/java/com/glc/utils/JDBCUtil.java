package com.glc.utils;

import java.sql.*;
import java.util.LinkedList;
import java.util.Queue;

public class JDBCUtil {
    private static Queue<Connection> pool=new LinkedList<>();

    private static Driver driver;
    static {
        try {
            driver=new com.mysql.cj.jdbc.Driver();
            DriverManager.registerDriver(driver);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    private static String url="jdbc:mysql://localhost:3306/store?useUnicode=true&characterEncoding=utf8&serverTimezone=GMT%2B8&useSSL=false&allowPublicKeyRetrieval=true";
    private static String user="root";
    private static String password="jinhaolin";

    public static Connection getConnection(){
        if (pool.size()==0){
            try {
                pool.add(DriverManager.getConnection(url,user,password));
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return pool.poll();
    }
    public static void release(Connection connection){
        pool.add(connection);
    }
    public static void release( PreparedStatement ps,Connection connection){
        try {
            ps.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        pool.add(connection);
    }
}
