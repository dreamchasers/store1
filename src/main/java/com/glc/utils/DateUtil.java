package com.glc.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    private static final DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
    private static final DateFormat timeFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static String toDateString(Date date){
        return dateFormat.format(date);
    }
    public static String toTimeString(Date date){
        return timeFormat.format(date);
    }
}
